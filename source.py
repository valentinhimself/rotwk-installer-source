import tkinter as tk
from tkinter import ttk, filedialog
import winreg
import os

import requests
import urllib.parse
from threading import Thread
import hashlib
import ctypes
import sys

if not ctypes.windll.shell32.IsUserAnAdmin():
    # Re-run the program with admin privileges
    script = os.path.abspath(__file__)
    params = ' '.join(sys.argv[1:])
    ctypes.windll.shell32.ShellExecuteW(None, "runas", sys.executable, script, params, 1)
    sys.exit()

class GUI(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("ROTWK Updater")
        self.geometry("544x319")
        self.wm_attributes('-topmost', True)
        # self.wm_attributes('-transparentcolor', 'gray')

        self.current_page = 1

        # create page 1
        self.page1 = tk.Frame(self, bg="white")
        self.page1.grid(row=0, column=0, sticky="nsew")
        self.background1 = tk.PhotoImage(file=r"C:\GitLab-Runner\Tkinter-Designer-master\build\assets\frame0\image_1.png")
        self.background_label1 = tk.Label(self.page1, image=self.background1)
        self.background_label1.place(x=0, y=0, relwidth=1, relheight=1)
        
        self.select_folder_button = tk.Button(self.page1, text="Select Folder", command=self.select_folder)
        self.select_folder_button.grid(row=0, column=0, pady=(143, 112))
        
        self.path_entry = tk.Entry(self.page1, width=44, state='disabled', font=("TkDefaultFont", 13))
        self.path_entry.grid(row=0, column=1, columnspan=2, padx=(0, 240), pady=(143, 112))

        self.languages_button = tk.Button(self.page1, text="Languages", width=10, command=self.show_page2)
        self.languages_button.grid(row=1, column=0, padx=20, pady=(0, 0))
        
        self.update_button = tk.Button(self.page1, text="Update", width=10, command=self.update_game)
        self.update_button.grid(row=1, column=1, padx=0, pady=(0, 0))
        
        self.exit_button = tk.Button(self.page1, text="Exit", width=10, command=self.destroy)
        self.exit_button.grid(row=1, column=2, padx=245, pady=(0, 0))

        # create page 2 (languages)
        self.page2 = tk.Frame(self, bg="white")
        self.background2 = tk.PhotoImage(file=r"C:\GitLab-Runner\Tkinter-Designer-master\build\assets\frame0\image_1.png")
        self.background_label2 = tk.Label(self.page2, image=self.background2)
        self.background_label2.place(x=0, y=0, relwidth=1, relheight=1)
        
        self.russian_button = tk.Button(self.page2, text="Russian", width=10, command=lambda: self.pick_language("Russian"))
        self.russian_button.grid(row=0, column=0, padx=20, pady=(255, 0))
                
        self.english_button = tk.Button(self.page2, text="English", width=10, command=lambda: self.pick_language("English"))
        self.english_button.grid(row=1, column=0, padx=20, pady=(0, 0))
        
        self.back_button = tk.Button(self.page2, text="Back", width=10, command=self.show_page1)
        self.back_button.grid(row=1, column=1, padx=0, pady=(0, 0))
        
        self.exit_button = tk.Button(self.page2, text="Exit", width=10, command=self.destroy)
        self.exit_button.grid(row=1, column=2, padx=245, pady=(0, 0)) 
        
        
        # create page 3 (update)
        self.page3 = tk.Frame(self, bg="white")
        self.background3 = tk.PhotoImage(file=r"C:\GitLab-Runner\Tkinter-Designer-master\build\assets\frame0\image_1.png")
        self.background_label3 = tk.Label(self.page3, image=self.background3)
        self.background_label3.place(x=0, y=0, relwidth=1, relheight=1)
        
        # Add a progress bar
        self.progress_bar = ttk.Progressbar(self.page3, orient="horizontal", length=500, mode="determinate")
        self.progress_bar.pack(padx=(0,0), pady=(145, 0))
        
        self.file_label = tk.Label(self.page3, text="Test", font=("TkDefaultFont", 14), fg="white", bg="#a4a4a4", width=30)
        self.file_label.place(x=115, y=200)
                
        # Add exit button using pack
        self.exit_button = tk.Button(self.page3, text="Exit", width=10, command=self.destroy)
        self.exit_button.pack(side="bottom", padx=(427, 0), pady=(0, 12))
        
        #end of page 3
        
        self.game_folder_path = self.get_folder_from_reg()
        print(self.game_folder_path)
        self.selected_language = "EnglishLanguageFiles"
        self.set_path_text(self.game_folder_path) #put default text in the box
        
        self.show_page1()

    def show_page1(self):
        # self.selected_path_label = tk.Label(self, text="")
        # self.selected_path_label.grid(row=0, column=1, pady=(143, 112))  
        self.page2.pack_forget()
        self.page1.pack(fill="both", expand=True)
        self.current_page = 1
     
        # self.selected_path_label = tk.Label(self, text="")
        # self.selected_path_label.grid(row=0, column=1, pady=(143, 112))

    def show_page2(self):
        self.page1.pack_forget()
        self.page2.pack(fill="both", expand=True)
        self.current_page = 2
    
    def show_update_page(self):
        self.page1.pack_forget()
        self.page3.pack(fill="both", expand=True)
        self.current_page = 3
        initiate_update()
    
    def get_folder_from_reg(self):
        self.reg_path = r"SOFTWARE\Wow6432Node\Electronic Arts\Electronic Arts\The Lord of the Rings, The Rise of the Witch-king"
        try:
            # Open the registry key and read the "InstallPath" value
            self.reg_key = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, self.reg_path)
            self.game_folder_path = winreg.QueryValueEx(self.reg_key, "InstallPath")[0]
            return self.game_folder_path
        except OSError:
            return ""
        
    def select_folder(self):
        # Open the file dialog to select a directory
        self.folder_path = filedialog.askdirectory()
        self.game_folder_path = self.folder_path.replace('/', '\\') + '\\'
        print(self.game_folder_path)
        self.set_path_text(self.folder_path)
        # Update the path entry widget with the selected folder path
    
    def set_path_text(self, path):
        self.path_entry.config(state='normal')
        self.path_entry.delete(0, 'end')
        self.path_entry.insert(0, path)
        self.path_entry.config(state='disabled')
    
    def pick_language(self, language):
        print(f'{language}')
        self.selected_language = f'{language}LanguageFiles'
        self.languages_button.config(text=language)
        self.show_page1()
        
    def update_game(self):
        switch_page_thread = Thread(target=self.show_update_page)
        switch_page_thread.start()
        download_thread = Thread(target=initiate_update)       

def get_all_files(path):
    file_list = []
    for root, dirs, files in os.walk(path):
        for name in files:
            file_list.append(os.path.join(root, name))
    return file_list

    
def calculate_md5(file_path):
    # Initialize the hash object with the MD5 algorithm
    hash_md5 = hashlib.md5()
    with open(file_path, "rb") as f:
        # Read the file in chunks to avoid loading the whole file into memory
        for chunk in iter(lambda: f.read(4096), b""):
            # Update the hash object with the contents of the chunk
            hash_md5.update(chunk)
    # Return the hexadecimal representation of the hash
    return hash_md5.hexdigest()

def get_lang_files(json_url, page_num):
    
    lang_files_url = "&ref=languages"
    all_lang_files = []
    lang_files_to_download = []
    while True:
        json_lang_data = requests.get(json_url + str(page_num)+str(lang_files_url)).json()
        lang_page_files = [file['path'] for file in json_lang_data if file['type'] == 'blob']
        if not lang_page_files:
            break
        all_lang_files.extend(lang_page_files)
        page_num += 1  
    for lang_file in all_lang_files:
        if lang_file.startswith(updater_gui.selected_language):
            lang_files_to_download.append(lang_file)

    return lang_files_to_download

def get_repo_files(repo_url):
    # json_url_english = "https://gitlab.com/api/v4/projects/valentinhimself%2Frotwk-updater/repository/tree?recursive=true&per_page=1000&page=1&ref=languages"
    json_url = "https://gitlab.com/api/v4/projects/valentinhimself%2Frotwk-updater/repository/tree?recursive=true&per_page=1000&page="
    page_num = 1
    branch = "&ref=develop"
    repo_files = []
    lang_files = get_lang_files(json_url, page_num)
    print(len(lang_files))
   
    while True:
        updater_gui.file_label.config(text="Parsing GitLab files")
        json_data = requests.get(json_url + str(page_num) + branch).json()
        page_files = [file['path'] for file in json_data if file['type'] == 'blob']
        if not page_files:
            break
        repo_files.extend(page_files)
        page_num += 1
    # updater_gui.file_label.config(text="")    
    print(len(repo_files))
    return lang_files + repo_files
    

def get_missing_files(repo_files, game_files):
    missing_files = []
    for file in repo_files:
        if file.startswith(updater_gui.selected_language):
            full_path_file = updater_gui.game_folder_path + truncate_file(file).replace("/", "\\")
        else:
            full_path_file = updater_gui.game_folder_path + file.replace("/", "\\")
        if full_path_file not in game_files:
            missing_files.append(file)
    print(len(missing_files))
    
    return missing_files

def truncate_file(file):
    #split off language folder name#
    #only keep lang and what is inside of lang#
    return '/'.join(file.split('/')[1:])

def get_short_file_name(file_name):
    
    if len(file_name) > 14:
        return "..." + file_name[-15:]
    return file_name

def download_files(missing_files, repo_url):
    for i, file in enumerate(missing_files):
        if not updater_gui.selected_language:
            break
        if file.startswith(updater_gui.selected_language):
            file_url = repo_url + "/-/raw/languages/" + urllib.parse.quote(file, safe='')
            file = truncate_file(file)
            file_path = os.path.join(updater_gui.game_folder_path, file)
        else:    
            file_url = repo_url + "/-/raw/develop/" + urllib.parse.quote(file, safe='')
            file_path = os.path.join(updater_gui.game_folder_path, file)
        os.makedirs(os.path.dirname(file_path), exist_ok=True)
        with open(file_path, "wb") as f:
            f.write(requests.get(file_url).content)
        updater_gui.progress_bar["value"] = (i + 1) * 100 // len(missing_files)
        # Set the text of the file label to the name of the file being downloaded
        updater_gui.file_label.config(text="Downloading " + get_short_file_name(file))
    return 

def update_existing_files(repo_files, repo_url):
    for file in repo_files:
        updater_gui.file_label.config(text="Updating...")
        if file.endswith((".dat", ".other", ".ini", ".exe")):
            #get the file paths
            repo_file_path = repo_url + "/-/raw/main/" + urllib.parse.quote(file, safe='')
            game_file_path = os.path.join(updater_gui.game_folder_path, file.replace("/", "\\"))
            #calculate their hashes
            repo_file_md5 = hashlib.md5(requests.get(repo_file_path).content).hexdigest()
            game_file_md5 = calculate_md5(game_file_path)

            if game_file_md5 != repo_file_md5:
                with open(game_file_path, "wb") as f:
                    f.write(requests.get(repo_file_path).content)
                    updater_gui.file_label.config(text="Updating " + get_short_file_name(file))
    return

def initiate_update():
    updater_gui.progress_bar.start()
    repo_url = "https://gitlab.com/valentinhimself/rotwk-updater"
    repo_files = get_repo_files(repo_url)
    game_files = get_all_files(updater_gui.game_folder_path)
    missing_files = get_missing_files(repo_files, game_files)
    print(str(len(game_files)) + "-" + "game file length")
    updater_gui.progress_bar.stop()
    
    download_files(missing_files, repo_url)
    updater_gui.progress_bar.start()
    update_existing_files(repo_files, repo_url)
    updater_gui.progress_bar.stop()
    updater_gui.file_label.config(text="")
    tk.messagebox.showinfo("Finished", "Files have been downloaded successfully!")
    # Clear the file label after downloading is complete
    # updater_gui.file_label.config(text="")
 

updater_gui = GUI()
updater_gui.mainloop()

