import tkinter as tk
from tkinter import ttk, filedialog

import winreg
import os

class GUI(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("ROTWK Updater")
        self.geometry("544x319")
        self.wm_attributes('-topmost', True)
        # self.wm_attributes('-transparentcolor', 'gray')

        self.current_page = 1

        # create page 1
        self.page1 = tk.Frame(self, bg="white")
        self.page1.grid(row=0, column=0, sticky="nsew")
        self.background1 = tk.PhotoImage(file=r"C:\GitLab-Runner\Tkinter-Designer-master\build\assets\frame0\image_1.png")
        self.background_label1 = tk.Label(self.page1, image=self.background1)
        self.background_label1.place(x=0, y=0, relwidth=1, relheight=1)
        
        self.select_folder_button = tk.Button(self.page1, text="Select Folder", command=self.select_folder)
        self.select_folder_button.grid(row=0, column=0, pady=(143, 112))
        
        self.path_entry = tk.Entry(self.page1, width=44, state='disabled', font=("TkDefaultFont", 13))
        self.path_entry.grid(row=0, column=1, columnspan=2, padx=(0, 240), pady=(143, 112))

        self.languages_button = tk.Button(self.page1, text="Languages", width=10, command=self.show_page2)
        self.languages_button.grid(row=1, column=0, padx=20, pady=(0, 0))
        
        self.update_button = tk.Button(self.page1, text="Update", width=10, command=self.show_update_page)
        self.update_button.grid(row=1, column=1, padx=0, pady=(0, 0))
        
        self.exit_button = tk.Button(self.page1, text="Exit", width=10, command=self.destroy)
        self.exit_button.grid(row=1, column=2, padx=245, pady=(0, 0))

        # create page 2 (languages)
        self.page2 = tk.Frame(self, bg="white")
        self.background2 = tk.PhotoImage(file=r"C:\GitLab-Runner\Tkinter-Designer-master\build\assets\frame0\image_1.png")
        self.background_label2 = tk.Label(self.page2, image=self.background2)
        self.background_label2.place(x=0, y=0, relwidth=1, relheight=1)
        
        self.russian_button = tk.Button(self.page2, text="Russian", width=10, command=lambda: self.pick_language("Russian"))
        self.russian_button.grid(row=0, column=0, padx=20, pady=(255, 0))
                
        self.english_button = tk.Button(self.page2, text="English", width=10, command=lambda: self.pick_language("English"))
        self.english_button.grid(row=1, column=0, padx=20, pady=(0, 0))
        
        self.back_button = tk.Button(self.page2, text="Back", width=10, command=self.show_page1)
        self.back_button.grid(row=1, column=1, padx=0, pady=(0, 0))
        
        self.exit_button = tk.Button(self.page2, text="Exit", width=10, command=self.destroy)
        self.exit_button.grid(row=1, column=2, padx=245, pady=(0, 0)) 
        
        
        # create page 3 (update)
        self.page3 = tk.Frame(self, bg="white")
        self.background3 = tk.PhotoImage(file=r"C:\GitLab-Runner\Tkinter-Designer-master\build\assets\frame0\image_1.png")
        self.background_label3 = tk.Label(self.page3, image=self.background3)
        self.background_label3.place(x=0, y=0, relwidth=1, relheight=1)
        
        # Add a progress bar
        self.progress = ttk.Progressbar(self.page3, orient="horizontal", length=500, mode="determinate")
        self.progress.pack(padx=(0,0), pady=(145, 0))
        
        self.file_label = tk.Label(self.page3, text="Test", font=("TkDefaultFont", 14), fg="white", bg="gray")
        self.file_label.place(x=50, y=200)
        
        # Add exit button using pack
        self.exit_button = tk.Button(self.page3, text="Exit", width=10, command=self.destroy)
        self.exit_button.pack(side="bottom", padx=(427, 0), pady=(0, 12))
        
        #end of page 3
        
        self.game_folder_path = self.get_folder_from_reg()
        print(self.game_folder_path)
        self.selected_language = "";
        self.set_path_text(self.game_folder_path) #put default text in the box
        
        self.show_page1()

    def show_page1(self):
        # self.selected_path_label = tk.Label(self, text="")
        # self.selected_path_label.grid(row=0, column=1, pady=(143, 112))  
        self.page2.pack_forget()
        self.page1.pack(fill="both", expand=True)
        self.current_page = 1
     
        # self.selected_path_label = tk.Label(self, text="")
        # self.selected_path_label.grid(row=0, column=1, pady=(143, 112))

    def show_page2(self):
        self.page1.pack_forget()
        self.page2.pack(fill="both", expand=True)
        self.current_page = 2
    
    def show_update_page(self):
        print("yes")
        self.page1.pack_forget()
        self.page3.pack(fill="both", expand=True)
        self.current_page = 3
        
    def show_progress_bar(self):
        self.progress = ttk.Progressbar(self, orient="horizontal", mode="determinate")
        self.progress.place(relx=0.5, rely=0.45, relwidth=0.7, anchor="n")
    
    def get_folder_from_reg(self):
        self.reg_path = r"SOFTWARE\Wow6432Node\Electronic Arts\Electronic Arts\The Lord of the Rings, The Rise of the Witch-king"
        try:
            # Open the registry key and read the "InstallPath" value
            self.reg_key = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, self.reg_path)
            self.game_folder_path = winreg.QueryValueEx(self.reg_key, "InstallPath")[0]
            return self.game_folder_path
        except OSError:
            return ""
        
    def select_folder(self):
        # Open the file dialog to select a directory
        self.folder_path = filedialog.askdirectory()
        self.game_folder_path = self.folder_path.replace('/', '\\') + '\\'
        print(self.game_folder_path)
        self.set_path_text(self.folder_path)
        # Update the path entry widget with the selected folder path
    
    def set_path_text(self, path):
        self.path_entry.config(state='normal')
        self.path_entry.delete(0, 'end')
        self.path_entry.insert(0, path)
        self.path_entry.config(state='disabled')
    
    def pick_language(self, language):
        print(f'{language}')
        self.selected_language = f'{language}LanguageFiles'
        self.languages_button.config(text=language)
        self.show_page1()
        # self.show_progress_bar()

updater = GUI()
updater.mainloop()

